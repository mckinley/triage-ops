require 'yaml'
require 'active_support/core_ext/hash/indifferent_access'
require 'gitlab'

require_relative 'variant'

class ReadOnlyComClient
  COM_GITLAB_API = 'https://gitlab.com/api/v4'

  def initialize(project_path:, token:)
    raise ArgumentError, 'An API token is needed!' if token.nil?

    @project_path = project_path
    @client = Gitlab.client(endpoint: COM_GITLAB_API, private_token: token)
  end

  def info(text)
    puts "⛑ [#{self.class}] #{text} ⛑"
  end

  def pipeline_schedules
    client.pipeline_schedules(project_path)
  end

  def pipeline_schedule(schedule)
    client.pipeline_schedule(project_path, schedule.id)
  end

  def create_pipeline_schedule(params)
    info "Creating a pipeline schedule with: #{project_path}, #{params}..."
  end

  def edit_pipeline_schedule(schedule, params)
    info "Editing a pipeline schedule with: #{project_path}, #{schedule&.id}, #{params}..."
  end

  def create_pipeline_schedule_variable(schedule, params)
    info "Creating a pipeline schedule variable with: #{project_path}, #{schedule&.id}, #{params}..."
  end

  def edit_pipeline_schedule_variable(schedule, variable_key, params)
    info "Editing a pipeline schedule variable with: #{project_path}, #{schedule&.id}, #{variable_key}, #{params}..."
  end

  def delete_pipeline_schedule_variable(schedule, variable_key)
    info "Deleting a pipeline schedule variable with: #{project_path}, #{schedule&.id}, #{variable_key}..."
  end

  private

  attr_reader :project_path, :client
end

class ReadWriteComClient < ReadOnlyComClient
  def info(text)
    puts "[#{self.class}] #{text}"
  end

  def create_pipeline_schedule(params)
    super

    client.create_pipeline_schedule(project_path, params)
  end

  def edit_pipeline_schedule(schedule, params)
    super

    client.edit_pipeline_schedule(project_path, schedule.id, params)
  end

  def create_pipeline_schedule_variable(schedule, params)
    super

    client.create_pipeline_schedule_variable(project_path, schedule.id, params)
  end

  def edit_pipeline_schedule_variable(schedule, variable_key, params)
    super

    client.edit_pipeline_schedule_variable(project_path, schedule.id, variable_key, params)
  end

  def delete_pipeline_schedule_variable(schedule, variable_key)
    super

    client.delete_pipeline_schedule_variable(project_path, schedule.id, variable_key)
  end
end

module Schedule
  class Manager
    PROJECT_PATH = 'gitlab-org/quality/triage-ops'

    InvalidPipelineScheduleIdError = Class.new(ArgumentError)

    def initialize(options)
      @options = options
    end

    def sync!
      sync_expected_schedules!
      info ''
      disable_extra_schedules!
    end

    def schedules_specification
      @schedules_specification ||= begin
        raw_schedules_specification.flat_map do |project_path, config|
          config[:variants].map do |variant_config|
            Variant.new(project_path, config[:base], variant_config)
          end
        end
      end
    end

    private

    attr_reader :options, :token, :api_client

    def api_client
      @api_client ||= begin
        client_class = options.dry_run ? ReadOnlyComClient : ReadWriteComClient
        client_class.new(project_path: PROJECT_PATH, token: options.token)
      end
    end

    def raw_schedules_specification
      YAML.load_file(File.expand_path('../../pipeline-schedules.yml', __dir__)).deep_symbolize_keys!
    end

    def remote_schedules
      @remote_schedules ||= api_client.pipeline_schedules.auto_paginate
    end

    def find_pipeline_schedule(schedules, schedule_to_find, raise_on_unknown_id: true)
      return find_pipeline_schedule_by(schedules, schedule_to_find, :description) unless schedule_to_find.id

      found_schedule = find_pipeline_schedule_by(schedules, schedule_to_find, :id)
      return found_schedule if found_schedule

      if raise_on_unknown_id
        raise(InvalidPipelineScheduleIdError, "💥 Schedule with ID ##{schedule_to_find.id} couldn't be found! 💥")
      else
        find_pipeline_schedule_by(schedules, schedule_to_find, :description)
      end
    end

    def find_pipeline_schedule_by(schedules, schedule_to_find, attr)
      schedules.find { |schedule| schedule.public_send(attr) == schedule_to_find.public_send(attr) }
    end

    def sync_expected_schedules!
      schedules_specification.each do |schedule_variant|
        remote_schedule = find_pipeline_schedule(remote_schedules, schedule_variant)

        if remote_schedule
          info "ℹ️ Schedule ##{remote_schedule.id} `#{remote_schedule.description}` was found. ℹ️"
          sync_attributes!(remote_schedule, schedule_variant)
          sync_variables!(remote_schedule, schedule_variant)
        else
          create_params = schedule_variant.create_params
          info "🏗 Schedule `#{schedule_variant.description}` wasn't found, we'll create it. 🏗"

          remote_schedule = api_client.create_pipeline_schedule(create_params)
          sync_variables!(remote_schedule, schedule_variant)
        end
        info ''
      end
    end

    def sync_attributes!(remote_schedule, schedule_variant)
      Variant::PIPELINE_SCHEDULE_ATTRS.each do |attr|
        expected_attr_value = schedule_variant.public_send(attr)
        actual_attr_value = remote_schedule.public_send(attr)
        next if actual_attr_value == expected_attr_value

        info "\t🏗 Attribute `#{attr}` value '#{actual_attr_value}' will be corrected to '#{expected_attr_value}'. 🏗"

        api_client.edit_pipeline_schedule(remote_schedule, { attr => expected_attr_value })
      end
    end

    def sync_variables!(remote_schedule, schedule_variant)
      detailed_schedule_variables = {}
      if remote_schedule
        detailed_schedule = api_client.pipeline_schedule(remote_schedule)
        detailed_schedule_variables = detailed_schedule.variables
        detailed_schedule_variables
          .reject { |variable| schedule_variant.variables.include?(variable['key'].to_sym) }
          .each do |variable|
            info "\t🗑 Variable #{variable['key']}=#{variable['value']} isn't a managed variable, we'll remove it. 🗑"
            api_client.delete_pipeline_schedule_variable(detailed_schedule, variable['key'])
          end
      end

      schedule_variant.variables.each do |k, v|
        found_variable = detailed_schedule_variables.find { |variable| variable['key'].to_sym == k }

        if found_variable
          if found_variable['value'] != v.to_s
            info "\t🏗 Variable `#{k}` was found but its value '#{found_variable['value']}' will be corrected to '#{v}'. 🏗"
            api_client.edit_pipeline_schedule_variable(detailed_schedule, k.to_s, { value: v.to_s })
          end
        else
          create_params = { key: k.to_s, value: v.to_s }
          info "\t🏗 Variable '#{k}' wasn't found, we'll create it. 🏗"
          api_client.create_pipeline_schedule_variable(detailed_schedule, create_params)
        end
      end
    end

    def disable_extra_schedules!
      remote_schedules
        .reject { |remote_schedule| find_pipeline_schedule(schedules_specification, remote_schedule, raise_on_unknown_id: false) }
        .each do |remote_schedule|

        info "⚠️ Schedule ##{remote_schedule.id} `#{remote_schedule.description}` isn't a managed schedule, it will be disabled. ⚠️"

        api_client.edit_pipeline_schedule(remote_schedule, { active: false })
      end
    end

    def info(text)
      puts text
    end
  end
end
